# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""The interaction between SQL and parquet."""

from io import BytesIO

import pyarrow as pa
import pyarrow.parquet as pq
import sqlalchemy as sa
from loguru import logger

from sync_client.data_models import DataLocation, DataPayload


def _get_sqlalchemy_type_from_pyarrow(  # noqa: C901, PLR0911
    pyarrow_dtype: pa.DataType,
) -> sa.types.TypeEngine:
    if pa.types.is_boolean(pyarrow_dtype):
        return sa.Boolean
    if (
        pa.types.is_int8(pyarrow_dtype)
        or pa.types.is_int16(pyarrow_dtype)
        or pa.types.is_int32(pyarrow_dtype)
        or pa.types.is_int64(pyarrow_dtype)
    ):
        return sa.Integer
    if (
        pa.types.is_uint8(pyarrow_dtype)
        or pa.types.is_uint16(pyarrow_dtype)
        or pa.types.is_uint32(pyarrow_dtype)
        or pa.types.is_uint64(pyarrow_dtype)
    ):
        return sa.Integer
    if (
        pa.types.is_float16(pyarrow_dtype)
        or pa.types.is_float32(pyarrow_dtype)
        or pa.types.is_float64(pyarrow_dtype)
    ):
        return sa.Float
    if pa.types.is_decimal(pyarrow_dtype):
        return sa.Numeric
    if pa.types.is_string(pyarrow_dtype) or pa.types.is_large_string(pyarrow_dtype):
        return sa.String
    if pa.types.is_binary(pyarrow_dtype) or pa.types.is_large_binary(pyarrow_dtype):
        return sa.LargeBinary
    if pa.types.is_date32(pyarrow_dtype) or pa.types.is_date64(pyarrow_dtype):
        return sa.Date
    if pa.types.is_timestamp(pyarrow_dtype):
        return sa.DateTime
    if pa.types.is_time32(pyarrow_dtype) or pa.types.is_time64(pyarrow_dtype):
        return sa.Time
    msg = f"Unknown data type {pyarrow_dtype}"
    raise ValueError(msg)


def _get_sql_table_from_pyarrow_table(
    table: pa.Table,
    table_name: str,
) -> sa.Table:
    schema = table.schema

    metadata = sa.MetaData()

    columns = []
    for field in schema:
        sqlalchemy_type = _get_sqlalchemy_type_from_pyarrow(field.type)

        columns.append(sa.Column(field.name, sqlalchemy_type))

    return sa.Table(table_name, metadata, *columns)


def _table_to_buffer(connection: sa.Connection, table_sql: sa.Table) -> BytesIO:
    """Read a sql table to a parquet buffer."""
    buffer = BytesIO()
    result = connection.execute(sa.select(table_sql))
    data = [row._asdict() for row in result]
    table = pa.Table.from_pylist(data)
    writer = pq.ParquetWriter(buffer, table.schema)
    writer.write_table(table)
    writer.close()
    buffer.seek(0)
    return buffer


def read_all_tables_as_parquet_buffer(
    engine: sa.Engine,
    *,
    excluded_tables: list[str],
) -> list[DataPayload]:
    """Dump all tables found in the DB engine as Parquet buffers."""
    meta = sa.MetaData()
    results = []
    with engine.begin() as conn:
        meta.reflect(bind=engine, views=True)
        for table_name in meta.tables:
            if table_name in excluded_tables:
                continue
            table_sql = sa.Table(table_name, meta, autoload_with=engine)

            logger.info(f"Dumping {table_name}")
            buffer = _table_to_buffer(conn, table_sql)
            results.append(
                DataPayload(
                    name=f"{table_name}.parquet",
                    data=buffer,
                    source=DataLocation.Database,
                ),
            )
    return results


def write_table_to_db(engine: sa.Engine, data_payload: DataPayload) -> bool:
    """Overwrite or create a table in parquet format in a io buffer."""
    table_arrow = pq.read_table(data_payload.data)
    table_sql = _get_sql_table_from_pyarrow_table(
        table_arrow,
        data_payload.name,
    )
    with engine.begin() as connection:
        table_sql.drop(connection, checkfirst=True)
        table_sql.create(connection)
        connection.execute(table_sql.insert(), table_arrow.to_pylist())
        connection.commit()
    logger.success(
        f"Wrote to DB Table {data_payload.name} size:"
        f" {len(data_payload.data.getvalue())/1024:.2f}kB",
    )
    return True
