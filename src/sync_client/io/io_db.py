# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Database interaction."""
import sqlalchemy as sa
from loguru import logger

from sync_client.config.app_config import AppConfig


def get_db_engine(connection_spec: AppConfig | str) -> sa.Engine:
    if isinstance(connection_spec, AppConfig):
        connection_spec = build_db_connection_string(
            username=connection_spec.Credentials.db_username,
            password=connection_spec.Credentials.db_password,
            database=connection_spec.Database.database,
            protocol=connection_spec.Database.protocol,
            host=connection_spec.Database.host,
            port=connection_spec.Database.port,
            driver=connection_spec.Database.driver,
            query=connection_spec.Database.query,
        )
    engine = sa.create_engine(connection_spec, pool_size=5, pool_pre_ping=True)
    logger.info(f"Test DB connection {engine.url}...")
    with engine.connect():
        pass
    logger.success(
        f"Created DB connection ({engine.url.drivername}) for:"
        f" {engine.url.host or engine.url}:{engine.url.port or ''}"
    )

    return engine


def build_db_connection_string(  # noqa: PLR0913
    username,
    password,
    database,
    protocol,
    host,
    port=None,
    driver=None,
    query=None,
) -> str:
    default_ports = {
        "mysql": "3306",
        "postgresql": "5432",
        "sqlite": None,
        "oracle": "1521",
        "mssql": "1433",
    }

    default_protocols = {
        "mysql": "mysql",
        "postgresql": "postgresql+pg8000",  # todo is this the correct protocol?
        "sqlite": "sqlite",
        "oracle": "oracle",
        "mssql": "mssql+pyodbc",
    }

    if protocol not in default_ports:
        msg = f"Unsupported protocol: {protocol}"
        raise ValueError(msg)

    if port is None or port == "":
        port = default_ports[protocol]

    query_parsed = _parse_query(query=query)
    if protocol == "sqlite":
        query_parsed.update({"check_same_thread": "False"})
        if host.startswith("/"):
            host = f"/{host}"
    elif protocol == "mssql":
        if driver is None:
            driver = "ODBC Driver 17 for SQL Server"
        query_parsed.update({"driver": driver})

    return sa.URL.create(
        default_protocols[protocol],
        username=username,
        password=password,
        host=host,
        port=port,
        database=database,
        query=query_parsed,
    ).render_as_string()


def _parse_query(query: str) -> dict:
    """
    Parse query from settings.ini to dictionary.

    Input: "TrustServerCertificate=YES&Trusted_connection=No"
    Output: {"TrustServerCertificate": "YES", "Trusted_connection": "No"}.
    """
    query_parsed = {}
    if query is None:
        return query_parsed
    for entry in query.split("&"):
        if "=" not in entry:
            msg = f"Wrong query format. {entry}"
            raise ValueError(msg)
        key, value = entry.split("=")
        query_parsed[key.strip()] = value.strip()

    return query_parsed
