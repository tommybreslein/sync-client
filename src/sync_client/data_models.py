# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""The local side settings for the sync client."""
from dataclasses import dataclass
from enum import IntEnum
from io import BytesIO
from pathlib import Path


def read_file_to_buffer(file_path: Path) -> BytesIO:
    buffer = BytesIO(file_path.read_bytes())
    buffer.seek(0)
    return buffer


class DataLocation(IntEnum):
    """Enumeration representing different data storage locations."""

    S3 = 0
    Database = 1
    LocalFile = 2


@dataclass
class DataPayload:
    """Represents a data payload with its associated metadata."""

    data: BytesIO
    name: str
    source: DataLocation

    @classmethod
    def from_file(cls, path: Path | str, name=None):
        data = read_file_to_buffer(Path(path))
        remote_name = name or path.name
        return DataPayload(data, remote_name, DataLocation.LocalFile)
