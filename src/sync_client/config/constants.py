# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Configuration settings not configurable by the user."""

# The optimized schedule that will be written to the DB.
schedule_table = "schedule"
schedule_file = "release/current_plan.parquet"

# Settings for the DB trigger table
trigger_column = "active"

# Error log file paths
remote_error_log = "error.log"
local_error_log = "sync_client_logs/error.log"

# s3 config
ingest_folder_prefix = "ingest/"

aws_region = "eu-central-1"
