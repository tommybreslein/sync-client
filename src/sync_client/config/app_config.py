# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Read and validate the settings from an INI file."""

import configparser
import logging
import sys
from pathlib import Path

from loguru import logger
from pydantic import (
    BaseModel,
    BaseSettings,
    Extra,
    ValidationError,
    root_validator,
    validator,
)

from sync_client.config import constants


class ClientSettings(BaseModel):
    class Config:  # noqa: D106
        extra = Extra.forbid


class FileSyncConfig(ClientSettings):
    sync_path: Path
    filenames: list[str]

    @validator("filenames", pre=True)
    def split_filenames(cls, value):  # noqa: N805
        if isinstance(value, str):
            return [s.strip() for s in value.split(",")]
        return value


class DBConfig(ClientSettings):
    database: str | None
    protocol: str
    host: str
    port: str | None = None
    trigger_table: str | None
    driver: str | None
    query: str | None = None


class CloudConfig(ClientSettings):
    bucket: str


class SecretsConfig(ClientSettings):
    secrets_path: Path | None = None


class CredentialsSettings(BaseSettings):
    aws_access_key_id: str
    aws_secret_access_key: str
    db_username: str | None
    db_password: str | None

    class Config:  # noqa: D106
        secrets_dir = ""


class AppConfigFromINI(ClientSettings):
    """A limited model to disallow setting credentials in the INI."""

    File_Sync: FileSyncConfig | None = None
    Database: DBConfig | None = None
    Cloud: CloudConfig
    Secrets: SecretsConfig | None = None

    @root_validator
    def at_least_one_source(cls, values):  # noqa: N805
        if values.get("File_Sync") is None and values.get("Database") is None:
            msg = (
                "At least one data source must be configured: a database or a sync file"
                " directory."
            )
            raise ValidationError(msg)
        return values


class AppConfig(AppConfigFromINI):
    Credentials: CredentialsSettings
    error_log_path: Path


def read_config_from_ini(file_path):
    parser = configparser.ConfigParser()
    parser.read(file_path)

    config_data = {}
    for section_name in parser.sections():
        config_data[section_name] = dict(parser[section_name])

    try:
        config_from_ini = AppConfigFromINI(**config_data)
        secrets_path = config_from_ini.Secrets.secrets_path

        config_args = config_from_ini.dict()
        config_args["Credentials"] = CredentialsSettings(
            _secrets_dir=secrets_path
        ).dict()
        config_args["error_log_path"] = Path.cwd() / constants.local_error_log

        config = AppConfig.parse_obj(config_args)
    except ValidationError as e:
        errors = "\n" + "\n".join(
            [f"{':'.join(error['loc'])} -> {error['msg']}" for error in e.errors()],
        )
        msg = f"INI file validation errors: {errors}"
        logging.error(msg)
        sys.exit(1)

    return config


def find_settings_file():
    script_directory = Path(__file__).parents[3]

    """Finds the settings.ini file in either the 'config'
      directory or the main directory of the tool"""
    config_path = script_directory / "config" / "settings.ini"
    if config_path.exists():
        return config_path

    fallback_path = script_directory / "settings.ini"
    if fallback_path.exists():
        return fallback_path

    return None


def get_settings() -> AppConfig:
    ini_file_path = find_settings_file()
    if ini_file_path is None:
        logger.error(
            "No settings file found."
            " Place settings.ini in the main directory of the tool."
        )
        sys.exit(1)

    logger.info(f"Loading INI from {ini_file_path}")
    return read_config_from_ini(ini_file_path)
