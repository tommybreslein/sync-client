# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""functions to configure the logging."""

import logging

from loguru import logger


class InterceptHandler(logging.Handler):
    """a handler to add python std lib logger to loguru."""

    def emit(self, record):
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        logger.opt(depth=6, exception=record.exc_info).log(level, record.getMessage())


def setup_logger(error_log_path):
    logging.getLogger("rocketry.scheduler").addHandler(InterceptHandler())
    logging.getLogger("rocketry.task").addHandler(InterceptHandler("WARNING"))
    logger.add(
        error_log_path,
        level="ERROR",
        rotation="10 MB",
        backtrace=True,
        diagnose=True,
    )
