# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""The main object of the sync client that pulls the functionality together."""
from pathlib import Path

import sqlalchemy as sa
from loguru import logger

from sync_client.config import constants
from sync_client.data_models import DataLocation, DataPayload
from sync_client.io import (
    db_parquet_adapter,
    io_s3,
)
from sync_client.scheduler import RetryTaskScheduler


class SyncClient:
    """
    Synchronous client for data synchronization between different destinations.

    This class acts as a data synchronization client that manages the transfer
    of data payloads between various destinations such as S3 and a database.
    It uses a scheduler with retry functionality to execute different tasks,
    including data polling, uploading, and database writing.

    Args:
    ----
        s3: An S3 bucket instance for handling S3 uploads.
        db_engine: SQLAlchemy engine for database connection.
        sources: List of data transfer sources, objects must implement the SourceMixIn
        locations_to_upload_to_s3: Set of data sources (that is locations) that will be
            uploaded to S3. Payloads from other triggers will be written to the DB
        data_source_to_trigger (DataLocation): Data source to trigger synchronization.
        scheduler: a task scheduler, that will execute and retry tasks.
        error_log_path (str | Path): Path to the error log file.

    """

    def __init__(  # noqa: PLR0913
        self,
        s3: io_s3.S3Bucket,
        db_engine: sa.Engine | None,
        sources: list,
        locations_to_upload_to_s3: set[DataLocation],
        scheduler: RetryTaskScheduler,
        error_log_path: str | Path,
    ):
        self.s3 = s3
        self.db_engine = db_engine
        self.sources = sources
        self.routing_table = self._get_routing_table(locations_to_upload_to_s3)
        self.error_log_path = error_log_path

        tasks = [
            (self.poll_sources, "every 5 seconds"),
            (self.upload_logs, "every 5 minutes"),
            (self.upload_to_s3, None),  # these will have to be added to the queue
            (self.write_to_db, None),  # these will have to be added to the queue
        ]

        self.scheduler = scheduler
        self.scheduler.add_tasks(tasks)

    # collect an setup payloads
    def notify(self, payload: DataPayload):
        """
        Notify the client about data transfers.

        Called by data transfer emitters.
        """
        self._schedule_transfer([payload])

    def poll_sources(self):
        """
        Poll data transfer sources for new updates.

        Called periodically by the scheduler
        """
        to_upload: list[DataPayload] = []

        for source in self.sources:
            to_upload.extend(source.poll())

        self._schedule_transfer(to_upload)

    # handle payloads
    def _schedule_transfer(
        self, to_upload: list[DataPayload], *, ingest_prefix: bool = True
    ):
        for payload in to_upload:
            prefix = constants.ingest_folder_prefix if ingest_prefix else ""

            match self.routing_table.get(payload.source, "unknown destination"):
                case DataLocation.S3:
                    self.scheduler.append_to_queue(
                        (
                            "upload_to_s3",
                            {"payload": payload, "prefix": prefix},
                        ),
                    )
                case DataLocation.Database:
                    self.scheduler.append_to_queue(
                        ("write_to_db", {"payload": payload}),
                    )
                case _:
                    logger.error(
                        "Unknown transfer"
                        f" destination:{self.routing_table.get(payload.source)}",
                    )

    def upload_to_s3(self, payload: DataPayload, prefix: str):
        remote_file = prefix + payload.name
        self.s3.upload_to_s3(payload.data, remote_file)

    def write_to_db(self, payload: DataPayload):
        db_parquet_adapter.write_table_to_db(
            self.db_engine,
            payload,
        )

    def _get_routing_table(
        self,
        to_s3_sources: set[DataLocation],
    ) -> dict[DataLocation, DataLocation]:
        """Create a from-to style routing table."""
        return {
            source: (
                DataLocation.S3 if source in to_s3_sources else DataLocation.Database
            )
            for source in DataLocation.__members__.values()
        }

    # maintenance
    def upload_logs(self):
        payload: DataPayload = DataPayload.from_file(
            self.error_log_path,
            name=constants.remote_error_log,
        )
        self._schedule_transfer([payload], ingest_prefix=False)

    def run(self):
        """Start the scheduler."""
        logger.success("Starting the sync client")
        self.scheduler.run()
