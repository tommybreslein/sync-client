# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Configure and run the sync tool."""

from loguru import logger

from sync_client.client_app import SyncClient
from sync_client.config import app_config, constants, logging_conf
from sync_client.data_models import DataLocation
from sync_client.io import io_db, io_s3
from sync_client.scheduler import RetryTaskScheduler
from sync_client.sources import file_watcher
from sync_client.sources.db_trigger import DBTriggerSource
from sync_client.sources.s3_trigger import S3TriggerSource

if __name__ == "__main__":
    # configure client
    settings = app_config.get_settings()

    logging_conf.setup_logger(settings.error_log_path)

    s3 = io_s3.S3Bucket(
        secret_key=settings.Credentials.aws_secret_access_key,
        access_key=settings.Credentials.aws_access_key_id,
        bucket_name=settings.Cloud.bucket,
        aws_region_name=constants.aws_region,
    )

    sources = []
    if settings.Database:
        db_engine = io_db.get_db_engine(settings)

        released_plan_source = S3TriggerSource(s3)
        sources.append(released_plan_source)

        if settings.Database.trigger_table:
            db_event_source = DBTriggerSource(
                db_engine,
                settings.Database.trigger_table,
                constants.trigger_column,
            )
            sources.append(db_event_source)
    else:
        db_engine = None

    # configure sync client
    sync_client = SyncClient(
        s3=s3,
        db_engine=db_engine,
        sources=sources,
        locations_to_upload_to_s3={DataLocation.Database, DataLocation.LocalFile},
        scheduler=RetryTaskScheduler(max_retries=4),
        error_log_path=settings.error_log_path,
    )

    if settings.File_Sync:
        file_observer = file_watcher.get_observer(
            settings.File_Sync,
            listener=[sync_client],
        )
        file_observer.start()

    # enter main loop
    try:
        sync_client.run()
    except KeyboardInterrupt:
        logger.info("Shutting down client")
