# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Create a observer for a directory that notifies a listener of changed files."""
import contextlib
import time
from pathlib import Path

from loguru import logger
from watchdog.events import FileSystemEvent, FileSystemEventHandler
from watchdog.observers import Observer

from sync_client.config.app_config import FileSyncConfig
from sync_client.data_models import DataPayload
from sync_client.exceptions import FileUnstableError


class ModifiedFileWatcher(FileSystemEventHandler):
    """
    Watch for modifications in files in a directory and notify a listener of changes.

    Args:
    ----
        directory_to_watch: The path of the directory to be watched.
        included_filenames: A set of filenames to consider for monitoring.
        listener: List of objects implementing the listener interface to notify.
        sec_wait_for_stable_file: Time (in seconds) to wait for a file to stabilize
          after modification, before triggering a changed file event.
    """

    def __init__(
        self,
        *,
        directory_to_watch: str,
        included_filenames: set[str],
        listener: list | None,  # todo define interface
        sec_wait_for_stable_file=15,
    ):
        self.directory_to_watch = Path(directory_to_watch)
        self.included_filenames = set(included_filenames)
        self.listener = listener or []
        self.sec_wait_for_stable_file = sec_wait_for_stable_file

        self.notify_all_initially_present_files()

    def on_modified(self, event: FileSystemEvent):
        """File is modified check if it can be uploaded."""
        self.notify(Path(event.src_path))

    def on_created(self, event: FileSystemEvent):
        """File is newly created check if it can be uploaded."""
        self.notify(Path(event.src_path))

    def notify_all_initially_present_files(self):
        """Check all files in the watched directory if they need to be uploaded."""
        for file_path in self.directory_to_watch.glob("*"):
            self.notify(file_path)

    @logger.catch(
        Exception,
        message="An error occurred while sending a file event.",
    )
    def notify(self, file_path: Path):
        """Notify listeners about a modified file."""
        with contextlib.suppress(IsADirectoryError):
            if file_path.name not in self.included_filenames:
                return
            self._wait_for_file_to_be_stable(file_path)
            for listener in self.listener:
                listener.notify(DataPayload.from_file(file_path))

    def _wait_for_file_to_be_stable(self, file_path: Path) -> bool:
        prev_size = file_path.stat().st_size

        for _ in range(self.sec_wait_for_stable_file):
            time.sleep(1)
            curr_size = file_path.stat().st_size
            if prev_size == curr_size:
                return True
            prev_size = curr_size

        msg = (
            f"File {file_path} size not stable after"
            f" {self.sec_wait_for_stable_file} seconds."
        )
        raise FileUnstableError(msg)


def get_observer(settings: FileSyncConfig, listener: list | None) -> Observer:
    """Create an observer instance for file monitoring."""
    event_handler = ModifiedFileWatcher(
        directory_to_watch=settings.sync_path,
        included_filenames=settings.filenames,
        listener=listener,
    )
    observer = Observer()
    observer.schedule(event_handler, settings.sync_path, recursive=False)
    logger.success(
        f"Started file watcher source for directory: {settings.sync_path} and files"
        f" {','.join(settings.filenames)}"
    )
    return observer
