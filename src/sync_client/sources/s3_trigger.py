# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""A Source for the SyncClient that triggers a poll based on a changed S3 file."""
from loguru import logger

from sync_client.config import constants
from sync_client.data_models import DataLocation, DataPayload
from sync_client.sources.source_mixin import SourceMixIn


class S3TriggerSource(SourceMixIn):
    """A trigger polling for the existence of a specific file."""

    def __init__(self, s3) -> None:
        self.s3 = s3
        self.released_plan = constants.schedule_file
        logger.success(
            f"Started S3 source. bucket:{s3.bucket} file:{self.released_plan}"
        )

    def poll(self) -> list[DataPayload]:
        schedule = self.s3.download_from_s3(self.released_plan)
        if not schedule:
            return []
        self.s3.delete_file_from_s3(self.released_plan)
        return [DataPayload(schedule, constants.schedule_table, source=DataLocation.S3)]
