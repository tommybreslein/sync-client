# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""A source monitoring a DB."""
import sqlalchemy as sa
from loguru import logger

from sync_client.config import constants
from sync_client.data_models import DataPayload
from sync_client.io import db_parquet_adapter
from sync_client.sources.source_mixin import SourceMixIn


class DBTriggerSource(SourceMixIn):
    """
    A data source monitoring a database for changes using a trigger table.

    Once a change is triggered the whole DB is fetched and passed to a polling obj.

    Args:
    ----
        trigger_table_name: Name of the database table that holds trigger information.
        trigger_column_name: Name of the column in the trigger table indicating
                             activation status.

    """

    def __init__(
        self,
        engine: sa.Engine,
        trigger_table_name: str,
        trigger_column_name: str,
    ):
        self.db_engine = engine

        self.trigger_table_name = trigger_table_name
        self.trigger_column_name = trigger_column_name
        self.excluded_tables = [trigger_table_name, constants.schedule_table]

        meta = sa.MetaData()
        self.trigger_table = sa.Table(
            self.trigger_table_name,
            meta,
            sa.Column(self.trigger_column_name, sa.Boolean),
        )
        logger.success(
            f"Started DB source trigger_table: `{self.trigger_table_name}`"
            f" db:`{self.db_engine.url}`"
        )

    def _is_trigger_active(self) -> bool:
        with self.db_engine.begin() as connection:
            stmt = sa.select(self.trigger_table).where(
                self.trigger_table.columns[self.trigger_column_name],
            )
            result = connection.execute(stmt).fetchall()
            return len(result) > 0

    def _deactivate_trigger(self) -> bool:
        with self.db_engine.begin() as connection:
            delete_stmt = self.trigger_table.delete()
            connection.execute(delete_stmt)
            insert_stmt = self.trigger_table.insert().values(
                **{self.trigger_column_name: False},
            )
            connection.execute(insert_stmt)
            logger.info("Deactivated DB trigger.")
            return True

    def poll(self) -> list[DataPayload]:
        """Poll for trigger activation, fetch updated data from the database."""
        if not self._is_trigger_active():
            return []
        logger.info("DB Trigger detected")

        to_upload: list[
            DataPayload
        ] = db_parquet_adapter.read_all_tables_as_parquet_buffer(
            self.db_engine,
            excluded_tables=self.excluded_tables,
        )

        self._deactivate_trigger()

        return to_upload
