"""
Packaging with cx_freeze is configured with this file.

Dependencies are automatically detected, but explicit defined packages are sometimes
needed to make cx_freeze work.
"""
from cx_Freeze import Executable, setup

build_options = {
    "packages": [
        "loguru",
        "sqlalchemy.dialects",
        "pyodbc",
        "os",
        "urllib3",
        "ssl",
        "boto3",
    ],
    "excludes": [
        "ruff",
        "moto",
        "pytest",
        "cx-freeze",
        "pytest-asyncio",
        "pytest-cov",
        "black",
    ],
    "include_files": [
        ("config/settings.ini.template", "settings.ini"),
        ("README.md", "README.md"),
        ("LICENSE", "LICENSE"),
        ("install_client_as_service.bat", "install_client_as_service.bat"),
        ("remove_client_as_service.bat", "remove_client_as_service.bat"),
        ("nssm", "nssm"),
    ],
}

base = "console"

executables = [
    Executable("src/sync_client/main.py", base=base, target_name="sync-tool")
]

setup(
    name="sync-tool",
    version="1.1.0",
    description=(
        "Sync Tool - Data Transfer and Synchronization Utility for anacision Planning"
    ),
    options={"build_exe": build_options},
    executables=executables,
)
