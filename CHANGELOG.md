# Changelog Sync Client

### Instructions Changelog

Code version is incremented with [Semantic Versioning](https://semver.org/).

Template:

```
## x.y.z - mm/dd/yyyy

#### Breaking changes

- Breaking change

#### Feature

- New feature 1
- New feature 2

#### Bugfix, CI-Check

- Bugfix 1
- Applied linting, black

```

---

## 1.1.0 - 09/13/2023

#### Feature

- Updated Readme with firewall instructions and list of IP ranges.
- Added option to disable parts of the Sync Tool via configuration
- Enabled export of interface database tables using views
- Added region option to internal configuration.
- Added changelog.

---

## 1.0.0 - 09/04/2023

#### Feature

- Initial release for open source under GPLv3
