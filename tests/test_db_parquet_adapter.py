# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from io import BytesIO

import pyarrow as pa
import pyarrow.parquet as pq
import pytest
import sqlalchemy as sa
from sync_client.data_models import DataPayload
from sync_client.io import db_parquet_adapter, io_db


@pytest.fixture()
def db_engine(conn_str_to_tmp_db):
    return io_db.get_db_engine(conn_str_to_tmp_db)


def test_read_all_tables_as_parquet(db_engine, trigger_table_name):
    # Test reading all tables as parquet and assert the results
    results = db_parquet_adapter.read_all_tables_as_parquet_buffer(
        db_engine,
        excluded_tables=[trigger_table_name],
    )
    assert len(results) == 1
    payload = results[0]
    assert payload.name == "data_table.parquet"
    assert isinstance(payload.data, BytesIO)


def test_write_table_to_db(db_engine):
    # Sample parquet data to be written to the database
    data = {"id": [1, 2, 3], "name": ["John", "Jane", "Jake"]}
    table_arrow = pa.Table.from_pydict(data)
    table_name = "test_table"
    data_bytes_io = BytesIO()
    pq.write_table(table_arrow, data_bytes_io)

    db_parquet_adapter.write_table_to_db(
        db_engine,
        DataPayload(data=data_bytes_io, name=table_name, source=0),
    )

    # Verify if the data was written to the database table correctly
    with db_engine.begin() as connection:
        result = connection.execute(
            sa.text(f"SELECT * FROM {table_name}"),
        ).fetchall()
        assert len(result) == len(data["id"])
        for i, row_raw in enumerate(result):
            row = row_raw._asdict()
            assert row["id"] == data["id"][i]
            assert row["name"] == data["name"][i]


@pytest.mark.parametrize(
    ("pyarrow_dtype", "expected_sa_type"),
    [
        (pa.bool_(), sa.Boolean),
        (pa.int8(), sa.Integer),
        (pa.int16(), sa.Integer),
        (pa.int32(), sa.Integer),
        (pa.int64(), sa.Integer),
        (pa.uint8(), sa.Integer),
        (pa.uint16(), sa.Integer),
        (pa.uint32(), sa.Integer),
        (pa.uint64(), sa.Integer),
        (pa.float16(), sa.Float),
        (pa.float32(), sa.Float),
        (pa.float64(), sa.Float),
        (pa.decimal128(precision=10, scale=2), sa.Numeric),
        (pa.string(), sa.String),
        (pa.large_string(), sa.String),
        (pa.binary(), sa.LargeBinary),
        (pa.large_binary(), sa.LargeBinary),
        (pa.date32(), sa.Date),
        (pa.date64(), sa.Date),
        (pa.timestamp("ns"), sa.DateTime),
        (pa.time32("ms"), sa.Time),
        (pa.time64("us"), sa.Time),
    ],
)
def test_get_sqlalchemy_type_from_pyarrow(pyarrow_dtype, expected_sa_type):
    result = db_parquet_adapter._get_sqlalchemy_type_from_pyarrow(pyarrow_dtype)
    assert result == expected_sa_type
