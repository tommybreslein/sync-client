# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from io import BytesIO

import boto3
import pytest
from moto import mock_s3
from sync_client.config import constants
from sync_client.data_models import DataLocation
from sync_client.io.io_s3 import S3Bucket
from sync_client.sources.s3_trigger import S3TriggerSource

# Replace 'your_module' with the actual module where S3TriggerSource is defined


ACCESS_KEY = "access_key"
SECRET_KEY = "secret_key"
BUCKET_NAME = "the_bucket_name"
AWS_REGION = "us-east-1"


@pytest.fixture()
def s3_bucket_w_schedule():
    with mock_s3():
        s3 = boto3.client("s3", region_name=AWS_REGION)
        s3.create_bucket(Bucket=BUCKET_NAME)
        s3.upload_fileobj(BytesIO(b"My schedule"), BUCKET_NAME, constants.schedule_file)
        yield S3Bucket(ACCESS_KEY, SECRET_KEY, BUCKET_NAME, AWS_REGION)


@pytest.fixture()
def s3_bucket():
    with mock_s3():
        s3 = boto3.client("s3", region_name=AWS_REGION)
        s3.create_bucket(Bucket=BUCKET_NAME)
        yield S3Bucket(ACCESS_KEY, SECRET_KEY, BUCKET_NAME, AWS_REGION)


def test_poll_with_schedule(s3_bucket_w_schedule):
    s3_trigger_source = S3TriggerSource(s3_bucket_w_schedule)

    result = s3_trigger_source.poll()

    assert len(result) == 1
    assert result[0].data.getbuffer() == BytesIO(b"My schedule").getbuffer()
    assert result[0].name == constants.schedule_table
    assert result[0].source == DataLocation.S3

    # Check if the schedule file is deleted
    assert "Contents" not in s3_bucket_w_schedule.s3.list_objects(Bucket=BUCKET_NAME)


def test_poll_without_schedule(s3_bucket):
    s3_trigger_source = S3TriggerSource(s3_bucket)
    # Perform the poll when no schedule file is available
    result = s3_trigger_source.poll()

    # Assertion
    assert result == []
