# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pyarrow.parquet as pq
import pytest
import sqlalchemy as sa
from sync_client.config import constants
from sync_client.io import io_db
from sync_client.sources.db_trigger import DBTriggerSource


@pytest.fixture()
def db_trigger(conn_str_to_tmp_db, trigger_table_name):
    return DBTriggerSource(
        io_db.get_db_engine(conn_str_to_tmp_db),
        trigger_table_name,
        constants.trigger_column,
    )


def test_check_trigger(db_trigger):
    # Add test data to the trigger table
    with db_trigger.db_engine.begin() as connection:
        insert_stmt = db_trigger.trigger_table.insert().values(
            **{db_trigger.trigger_column_name: True},
        )
        connection.execute(insert_stmt)

    is_triggered = db_trigger._is_trigger_active()
    assert is_triggered


def test_table_not_found_check_trigger(db_trigger, trigger_table_name):
    # Add test data to the trigger table
    with db_trigger.db_engine.begin() as connection:
        db_trigger.trigger_table.drop(connection)
    with pytest.raises(
        sa.exc.OperationalError,
        match=f"no such table: {trigger_table_name}",
    ):
        db_trigger._is_trigger_active()


def test_poll(db_trigger):
    """Check if polling resets the trigger and returns the data."""
    with db_trigger.db_engine.begin() as connection:
        insert_stmt = db_trigger.trigger_table.insert().values(
            **{db_trigger.trigger_column_name: True},
        )
        connection.execute(insert_stmt)

    result = db_trigger.poll()

    assert len(result) == 1
    assert result[0].name == "data_table.parquet"

    is_triggered = db_trigger._is_trigger_active()
    assert not is_triggered
    table_arrow = pq.read_table(result[0].data)
    assert table_arrow.column_names == ["id", "name"]
