# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import threading
import time
from unittest.mock import MagicMock

import pytest
from sync_client.exceptions import FileUnstableError
from sync_client.sources.file_watcher import ModifiedFileWatcher
from watchdog.observers import Observer


def test_on_modified(tmp_path):
    """Check if watchdog call the the on_modified method."""
    event_handler = ModifiedFileWatcher(
        directory_to_watch=tmp_path,
        included_filenames={"file1.txt", "file2.txt"},
        listener=None,
    )

    event_handler.notify = MagicMock()

    observer = Observer()
    observer.schedule(event_handler, tmp_path, recursive=False)
    observer.start()
    time.sleep(0.3)  # give the observer time to start
    test_file = tmp_path / "file1.txt"
    test_file.write_text("Hello observer")
    time.sleep(0.3)  # give the observer time detect changes

    observer.stop()

    event_handler.notify.assert_any_call(test_file)


def test_check_directory_for_uploads(tmp_path):
    """Check if the correct number of files are uploaded."""
    listener = MagicMock()
    event_handler = ModifiedFileWatcher(
        directory_to_watch=tmp_path,
        included_filenames={"file1.txt", "file2.txt"},
        listener=[listener],
    )
    files_in_directory = [
        "file1.txt",
        "file2.txt",
        "file3.txt",
    ]
    for f in files_in_directory:
        (tmp_path / f).touch()

    event_handler.notify_all_initially_present_files()

    # Check if excluded the files not in the settings
    assert {
        payload[0][0].name for payload in list(listener.notify.call_args_list)
    } == set(files_in_directory[:2])


def test_wait_for_file_to_be_stable_raises_error(tmp_path):
    """Test if files with an unstable file size can really be detected."""

    def write_to_file(file_path, duration):
        with file_path.open("r+") as f:
            for _n in range(duration):
                # Modify the file content to simulate size changes
                f.write("additional content\n")
                f.flush()
                time.sleep(0.3)

    watcher = ModifiedFileWatcher(
        directory_to_watch=tmp_path,
        included_filenames=set(),
        sec_wait_for_stable_file=1,
        listener=None,
    )

    file_path = tmp_path / "file1.txt"
    file_path.touch()
    duration = 2
    writer_thread = threading.Thread(target=write_to_file, args=(file_path, duration))
    writer_thread.start()

    with pytest.raises(
        FileUnstableError,
        match=f"File {file_path} size not stable after 1 seconds.",
    ):
        watcher._wait_for_file_to_be_stable(file_path)
