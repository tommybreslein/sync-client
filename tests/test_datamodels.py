# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sync_client.data_models import DataLocation, DataPayload


def test_data_payload_from_file(tmp_path):
    content = b"test file content"
    tmp_file = tmp_path / "test_file.txt"
    tmp_file.write_bytes(content)
    name = "my_name"

    payload = DataPayload.from_file(tmp_file, name)
    assert payload.data.read() == content
    assert payload.name == name
    assert payload.source == DataLocation.LocalFile
