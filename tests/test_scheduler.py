# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from rocketry.conditions.scheduler import SchedulerCycles
from sync_client.scheduler import RetryTaskScheduler


@pytest.fixture()
def scheduler():
    return RetryTaskScheduler()


def test_run_task_in_queue_success(scheduler):
    # Create a mock task and test its successful execution

    def mock_task():
        pass

    scheduler.add_tasks([(mock_task, None)])
    scheduler.append_to_queue(("mock_task", {}))

    scheduler.rocketry.session.config.shut_cond = SchedulerCycles() >= 3
    scheduler.rocketry.session.start()

    assert len(scheduler.task_queue) == 0, "The task should be removed from the queue"
    # todo this test only works partially we do not check in if a task is actually run.
