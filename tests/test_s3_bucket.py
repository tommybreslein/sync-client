# SyncTool: transfer data to and from local files and DB to AWS S3.
# Copyright (C) 2023  anacision GmbH

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from io import BytesIO

import boto3
import pytest
from moto import mock_s3
from sync_client.io.io_s3 import (
    S3Bucket,
)

ACCESS_KEY = "access_key"
SECRET_KEY = "secret_key"
BUCKET_NAME = "the_bucket_name"
AWS_REGION = "us-east-1"


@pytest.fixture()
def s3_bucket():
    with mock_s3():
        s3 = boto3.client("s3", region_name=AWS_REGION)
        s3.create_bucket(Bucket=BUCKET_NAME)
        yield S3Bucket(ACCESS_KEY, SECRET_KEY, BUCKET_NAME, AWS_REGION)


def test_upload_to_s3(s3_bucket, caplog):
    local_data = BytesIO(b"Hello, World!")
    remote_path = "test_upload.txt"

    result = s3_bucket.upload_to_s3(local_data, remote_path)
    assert result is True

    contents = s3_bucket.s3.list_objects(Bucket=BUCKET_NAME)["Contents"]
    assert len(contents) == 1
    assert contents[0]["Key"] == remote_path
    etag_first = contents[0]["ETag"].replace('"', "")

    # Perform the second upload with the same data, it should not upload again
    result = s3_bucket.upload_to_s3(local_data, remote_path)
    assert result is False

    # Perform the third upload with different data, it should upload again
    local_data = BytesIO(b"New Data")
    result = s3_bucket.upload_to_s3(local_data, remote_path)
    assert result is True

    contents = s3_bucket.s3.list_objects(Bucket=BUCKET_NAME)["Contents"]
    assert len(contents) == 1
    assert contents[0]["Key"] == remote_path
    etag_last = contents[0]["ETag"].replace('"', "")
    assert etag_last != etag_first


def test_are_equal_local_and_remote_identical_data(s3_bucket):
    local_data = BytesIO(b"Hello, World!")
    remote_path = "test_upload.txt"
    s3_bucket.upload_to_s3(local_data, remote_path)

    local_data2 = BytesIO(b"Hello, World!")

    assert s3_bucket._are_equal_local_and_remote(remote_path, local_data2) is True


def test_are_equal_local_and_remote_new_upload(s3_bucket):
    remote_path = "test_upload.txt"
    local_data2 = BytesIO(b"Hello, World!")

    assert s3_bucket._are_equal_local_and_remote(remote_path, local_data2) is False


def test_delete_to_s3(s3_bucket, caplog):
    local_data = BytesIO(b"Hello, World!")
    remote_path = "test_upload.txt"

    s3_bucket.upload_to_s3(local_data, remote_path)
    s3_bucket.delete_file_from_s3(remote_path)

    assert "Contents" not in s3_bucket.s3.list_objects(Bucket=BUCKET_NAME)


def test_download_from_s3(s3_bucket, caplog):
    local_data = BytesIO(b"Hello, World!")
    remote_path = "test_upload.txt"

    s3_bucket.upload_to_s3(local_data, remote_path)
    downloaded = s3_bucket.download_from_s3(remote_path)

    assert downloaded.getbuffer() == local_data.getbuffer()


def test_download_non_existing_file(s3_bucket, caplog):
    remote_path = "test_upload.txt"

    downloaded = s3_bucket.download_from_s3(remote_path)

    assert downloaded is None
