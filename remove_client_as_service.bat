::
:: Remove app as windows service
::
@echo off
:: base path of apps
set app_base_path=%~dp0
:: set app values
set start_type=SERVICE_DELAYED_AUTO_START
set nssm_exec="%app_base_path%nssm\nssm.exe"
set app_exec=sync-tool.exe
set service_name=Anacision.Planning.SyncClient

:: Remove services
ECHO Path to client exe: "%app_base_path%%app_exec%"

CALL sc delete %service_name%
