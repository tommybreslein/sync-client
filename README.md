<!-- SyncTool: transfer data to and from local files and DB to AWS S3.
Copyright (C) 2023  anacision GmbH

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. -->

# Sync Client - Data Transfer and Synchronization Utility

The Sync Client is an open-source utility developed by [**anacision**](https://www.anacision.de) to facilitate the seamless transfer of data to and from anacision PLANNING when using the adapter-service. 
This tool allows users to synchronize data from local sources, that is databases and individual files, making the integration of the planning service straightforward. 
The Sync Client is designed to be run on-premise, ensuring data security and control. 
This README provides an overview of the tool's features, setup instructions, and configuration options.


### Features

- **Data Source Variety**: The Sync Client supports two primary data sources:
  - **Source 1: Database (e.g., ERP-tabular data)**: Enables the transfer of data from a live database to the adapter service of anacision PLANNING. 
  - **Source 2: Local Files (e.g., Excel)**: Allows synchronization of local files from a predefined location.

- **Secure Communication**: The tool ensures secure data transfer through only outgoing SSL-encrypted communication. This feature safeguards sensitive information during transmission, and protects the host/on premise system from exposing itself to the public internet.

- **Easy integration**: The tool greatly simplifies the process of exchanging data with the anacision PLANNING adapter-service thus allowing for easy integration of anacision PLANNING. 


### Synchronization Process

The synchronization process of the Sync Client can be summarized as follows:

1. **Data Source Detection**: The tool identifies the configured data sources, including the interface database and the local files, based on the provided configuration.

2. **Database Synchronization**: If database synchronization is enabled, the tool connects to the interface database. It monitors the trigger table for changes and detects modifications to the data. When the `active` trigger is set to true, the tool initiates the upload of the entire interface database if the data has changed compared to the last upload.

3. **File Synchronization**: For file synchronization, the tool watches the specified files in the specified folder for any changes. When a new file is added or an existing file is modified, the tool promptly uploads the updated file.

4. **Download released plans**: As soon as the anacision adapter provides a released plan, the Sync Client downloads this plan and writes it to the `schedule` table in the interface database. This table will always contain the latest released schedule, older versions are overwritten. It is up to you further process the data in this table. 


### Secure Communication

Security is a fundamental aspect of the Sync Client's design. All communication between the tool and anacision PLANNING occurs over an SSL-encrypted channel. This encryption ensures that data remains confidential during transmission, minimizing the risk of unauthorized access or tampering.


---


## Content Readme

1. [Installation](#installation)
2. [Running the Tool](#running-tool)
3. [Working Principle](#working-principle)
4. [Getting Help](#getting-help)
5. [License](#license)
6. [Development and Releases](#development)


---


## 1. Getting Started  <a name="installation"></a>

## 1.1 Prerequisites

* Supported OS: Windows, Linux
* File synchronisation from local file system or network shares 
* Database table synchronisation from interface database
* Database table write to interface database

## 1.2 Installation

### Windows

 - Unzip the packaged Sync Client into a folder of your choice. 
 - Follow the instructions in the configuration section.
 - Additional resources for MSSQL database: 
   - [ODBC driver](https://learn.microsoft.com/en-us/sql/connect/odbc/download-odbc-driver-for-sql-server)
   - [Check ODBC driver version (if installed already)](https://learn.microsoft.com/en-us/previous-versions/windows/desktop/odbc/dn170514(v=vs.85))
 - Additional resources for missing C++ packages:
   - [Microsoft Visual C++ Redistributable Package](https://cx-freeze.readthedocs.io/en/latest/faq.html#microsoft-visual-c-redistributable-package)
 - Setup Sync Client as Windows Service (see section *Running as a Windows Service*) 

### Linux 

 - Unzip the packaged Sync Client into a folder of your choice. 
 - Follow the instructions in the configuration section.


## 1.3 Initial Configuration

To configure the Sync Client for your environment, follow these steps:

1. **Firewall setup**:Depending on your IT setup you might have to configure you firewall to allow outgoing communication [(See appendix A)](README.md#a-ip-ranges-for-firewall-config).


2. **Setup Interface Database Data Source**: The Sync Client allows data to be transferred from an interface database, specifically designed for data synchronization. This interface database contains relevant data from ERP/MES systems. It is crucial to create a new and dedicated database for this purpose, separate from the production database. Most standard SQL databases are supported for this setup. To establish this data source, follow these steps:

   - Create the Interface Database: Set up a new database instance exclusively for data synchronization. 
     Ensure that the tool has read and write access.
   - Write Data Transfer Scripts: Develop scripts within your ERP/MES system to transfer data to the interface database in the agreed-upon format.
   - Configure Trigger Table: Within the interface database, create a trigger table with a boolean column named `active`. 
     This trigger indicates when the data has been updated and should be uploaded. 
     Write the trigger appropriately in your update scripts.


3. **Synchronization**: The synchronization process is controlled by the `active` trigger. When the trigger is set to true, the entire interface database is uploaded, and the trigger is reset. 
   Note that sync-tool, will automatically create a table `schedule` where the latest released schedule from the anacision adapter is written to. You do not need to create the table.

   - Centralized File Storage: Store the relevant files in a central, local folder accessible to all relevant internal stakeholders.
   - Client Access: Ensure that the Sync Client has at least read access to the folder containing the files.
   - We recommend maintaining a centralized file location (e.g. network path) for easy access by internal stakeholders, ensuring smooth collaboration during the planning process.


4. **Edit the Configuration**: Open the `settings.ini` file to customize the tool's behavior. The configuration file is divided into several sections:

   - **File_Sync**: Configure the synchronization of local files.
     - `sync_path`: Provide the absolute path to the folder containing the data files to be synced.
     - `filenames`: List the filenames with extensions of the files to be synced, separated by commas.

   - **Database**: Configure the database connection.
     - `protocol`: Set the database protocol (e.g., sqlite).
     - `host`: Specify the IP address of the database host.
     - `port`: Set the database port. If not available, the default ports of the database protocol are used.
     - `database`: Set the database name.
     - `query`: Query parameters for connection string e.g. to specify MSSQL driver. Add multiple values with *'&'* e.g. *query=TrustServerCertificate=Yes&Trusted_connection=No*
     - `trigger_table`: Set the table to watch for changes using a trigger. If this value is not set the Database is disabled as a source that will be uploaded

   - **Cloud**: Configure cloud storage settings.
     - `bucket`: Specify the name of the cloud storage bucket to use. This will be provided by anacision

   - **Secrets**: Configure secrets management.
     - `secrets_path`: Provide the path to the folder containing secret files. Alternatively, secrets can be set via environment variables.

  It is possible to disable the difference parts of the sync tool: To completely disable file sync and DB, exclude the `File_Sync` or `Database` sections respectively in the config file. If you just want to disable the database as data source, but keep the downloading of results from S3  functionality you should include the `Database` section but without the `trigger_table` field.

4. **Secrets Management**: If using the `secrets_path` option, create files with the corresponding secret names (e.g., `aws_access_key_id`, `aws_secret_access_key`, `db_username`, `db_password`) within the secrets folder.

## 2. Running the Tool <a name="running-tool"></a>

The Sync Client needs to run continuously in the background, e.g. as a Windows service.

### 2.1 Running as a Windows Service

**Automatic Installation**

1. Run `install_client_as_service.bat` with admin privileges to install Sync Client as Windows service using the [NSSM tool](https://nssm.cc).
2. Open *Service/Microsoft Management Tool* and start service `Anacision.Planning.SyncClient`.

The Sync Client will now run as a Windows service, executing the synchronization process based on your configuration.

**Automatic Uninstallation**

1. Run `remove_client_as_service.bat` with admin privileges to remove Sync Client as Windows service.

**Manual Restart**

Open the Windows service app and search for the service `Anacision.Planning.SyncClient`. 
Via context menu the service can be started, stopped or restarted.

### 2.2 Supported Databases

| Version Client | OS      | Database | Tested |
|----------------|---------|----------|--------|
| 1.0            | Windows | SQLite   | yes    |
| 1.0            | Windows | MSSQL    | yes    |
| 1.0            | Windows | Postgres | no     |
| 1.0            | Windows | Oracle   | no     |
| 1.0            | Windows | MariaDB  | no     |
| 1.0            | Windows | MySQL    | no     |
| 1.0            | Linux   | SQLite   | yes    |
| 1.0            | Linux   | MSSQL    | no     |
| 1.0            | Linux   | Postgres | no     |
| 1.0            | Linux   | Oracle   | no     |

More database dialects should be functional, but have not been tested.
[See SQLAlchemy supported dialects.](https://docs.sqlalchemy.org/en/20/dialects/)

**MSSQL**

- One need to define a set of parameters via the connection string query parameter `query`.
  - **Driver:** Specify which ODBC driver to use for communication. Driver needs to be installed on client. 
  Most common values are version 17 or 18 e.g. `driver=ODBC Driver 17 for SQL Server`
  - **Encrypt:** ODBC driver version 18 changed default value to `Encrypt=True`, which activates TLS by default
  - **TrustServerCertificate:** Using self-signed certificates could require using `TrustServerCertificate=Yes`, which skips certificate check.
  - **Trusted_connection:** Specifies whether a user connects through a user account or another platform-specific authentication. 
  Setting to `Trusted_connection=No` uses credentials from connection string to log in to database. 
- Using SQL-User: 
  - Set to `query=driver=ODBC Driver 18 for SQL Server&TrustServerCertificate=Yes&Trusted_connection=No`
  - Set secrets `db_username` and `db_password`
- Using Windows-User: 
  - Set to `query=driver=ODBC Driver 18 for SQL Server&Trusted_connection=Yes`
  - Do not add secrets `db_username` and `db_password`. Alternative: keep files empty.
- Using self-signed certificates could require using `TrustServerCertificate=Yes`


---


## 4. Getting Help <a name="getting-help"></a>

If you encounter any issues or have questions about using the Sync Client, feel free to reach out to our [**support team**](mailto:planning-support@anacision.com). We value your feedback and contributions to this open-source project.

## 5. License <a name="license"></a>

This CLI tool is distributed under the terms of the GNU General Public License version 3 (GPLv3).

### GNU General Public License (GPLv3)

The full text of the GPLv3 license is available in the [LICENSE](LICENSE) file in the root directory of this project.

#### Permissions

- You are free to use, modify, and distribute this software in accordance with the terms of the GPLv3.

#### Obligations

- If you make modifications to this software, you must share those modifications under the same GPLv3 license.
- When distributing this software or any modifications, you must provide access to the corresponding source code.

### Contributions

Contributions to this project are welcome, but contributors must agree to license their contributions under the terms of the GPLv3.

For more details, please refer to the [LICENSE](LICENSE) file.

For questions or licensing inquiries, contact [**anacision GmbH**](mailto:planning-support@anacision.com).


---


## 6. Development and Releases <a name="development"></a>

**Build a new release**

- Package Sync Client with `cx_freeze`. 
Directory `build` is created with architecture subdirectory e.g `exe.win-amd64-3.11`.

        > python setup.py build

- Create zip file with naming convention `sync_tool-{version}-{architecture name from cx_freeze}.zip` e.g. `sync_tool-1.0.0-win-amd64-3.11.zip`

- Upload file and create a new *Release* via [GitLab Release](https://gitlab.com/anacision/sync-client/-/releases).

**Latest release**

One can find the latest release and a list of all releases [here](https://gitlab.com/anacision/sync-client/-/releases)

---

# Appendix:

## A: IP ranges for firewall config

Depending on your IT setup you might have to configure you firewall to all outgoing communication to the following IP ranges.
Allowing TCP over port 443 for HTTPS should be sufficient.


  | ip_prefix       | region       |
  |:----------------|:-------------|
  | 13.248.224.0/24 | GLOBAL       |
  | 13.248.225.0/24 | GLOBAL       |
  | 13.248.226.0/24 | GLOBAL       |
  | 13.248.227.0/24 | GLOBAL       |
  | 13.248.228.0/24 | GLOBAL       |
  | 13.248.229.0/24 | GLOBAL       |
  | 13.248.230.0/24 | GLOBAL       |
  | 13.248.231.0/24 | GLOBAL       |
  | 13.248.232.0/24 | GLOBAL       |
  | 13.248.233.0/24 | GLOBAL       |
  | 76.223.100.0/24 | GLOBAL       |
  | 76.223.101.0/24 | GLOBAL       |
  | 76.223.102.0/24 | GLOBAL       |
  | 76.223.103.0/24 | GLOBAL       |
  | 76.223.104.0/24 | GLOBAL       |
  | 76.223.95.0/24  | GLOBAL       |
  | 76.223.96.0/24  | GLOBAL       |
  | 76.223.97.0/24  | GLOBAL       |
  | 76.223.98.0/24  | GLOBAL       |
  | 76.223.99.0/24  | GLOBAL       |
  | 16.12.24.0/21   | eu-central-1 |
  | 16.12.32.0/22   | eu-central-1 |
  | 3.5.134.0/23    | eu-central-1 |
  | 3.5.136.0/22    | eu-central-1 |
  | 3.65.246.0/28   | eu-central-1 |
  | 3.65.246.16/28  | eu-central-1 |
  | 52.219.140.0/24 | eu-central-1 |
  | 52.219.168.0/24 | eu-central-1 |
  | 52.219.169.0/24 | eu-central-1 |
  | 52.219.170.0/23 | eu-central-1 |
  | 52.219.208.0/23 | eu-central-1 |
  | 52.219.210.0/24 | eu-central-1 |
  | 52.219.211.0/24 | eu-central-1 |
  | 52.219.218.0/24 | eu-central-1 |
  | 52.219.44.0/22  | eu-central-1 |
  | 52.219.72.0/22  | eu-central-1 |
  | 16.62.56.224/28 | eu-central-2 |
  | 16.62.56.240/28 | eu-central-2 |
  | 3.5.52.0/22     | eu-central-2 |
  | 52.95.139.0/24  | eu-central-2 |
  | 52.95.140.0/23  | eu-central-2 |


*Disclaimer: This document is intended for informational purposes only. All product and company names mentioned are trademarks™ or registered® trademarks of their respective holders. Use of these trademarks does not imply any affiliation with or endorsement by the respective holders.*
