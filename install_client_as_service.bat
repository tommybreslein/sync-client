::
:: Install app as windows service
::
@echo off
:: base path of apps
set app_base_path=%~dp0
:: set app values
set start_type=SERVICE_DELAYED_AUTO_START
set nssm_exec="%app_base_path%nssm\nssm.exe"
set app_exec=sync-tool.exe
set service_name=Anacision.Planning.SyncClient

:: Install services
ECHO Path to client exe: "%app_base_path%%app_exec%"

CALL %nssm_exec% install %service_name% "%app_base_path%%app_exec%"
CALL %nssm_exec% set %service_name% Description "Sync Tool - Data Transfer and Synchronization Utility for anacision Planning"
CALL %nssm_exec% set %service_name% Start %start_type%

:: Uncomment lines to add additional logging sinks for debugging
::CALL %nssm_exec% set %service_name% AppStdout "%app_base_path%/error_service.log"
::CALL %nssm_exec% set %service_name% AppStderr "%app_base_path%/error_service.log"
